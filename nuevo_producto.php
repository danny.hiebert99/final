<?php
// Configuración de la base de datos
define('DB_HOST', 'localhost');
define('DB_PORT', '5432');
define('DB_NAME', 'final');
define('DB_USER', 'postgres');
define('DB_PASSWORD', 'postgres');

session_start();

if (!isset($_SESSION['user_id'])) {
    header('Location: login.php');
    exit();
}

// Verificar si se envió el formulario para agregar un nuevo producto
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Validar y procesar los datos del formulario
    if (isset($_POST['nombre'], $_POST['codigo'], $_POST['marca'], $_POST['precio'])) {
        $nombre = $_POST['nombre'];
        $codigo = $_POST['codigo'];
        $marca = $_POST['marca'];
        $precio = $_POST['precio'];

        try {
            $pdo = new PDO("pgsql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $pdo->prepare("INSERT INTO productos (nombre, codigo, marca, precio) VALUES (:nombre, :codigo, :marca, :precio)");
            $stmt->bindParam(':nombre', $nombre);
            $stmt->bindParam(':codigo', $codigo);
            $stmt->bindParam(':marca', $marca);
            $stmt->bindParam(':precio', $precio);
            $stmt->execute();

            echo "Producto agregado correctamente.";
        } catch (PDOException $e) {
            echo "Error al agregar el producto: " . $e->getMessage();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar Nuevo Producto</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
            text-align: center;
        }

        h1 {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        form {
            display: inline-block;
            text-align: left;
            max-width: 300px;
            margin: auto;
        }

        label {
            display: block;
            margin-top: 10px;
        }

        input, button {
            padding: 8px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
        }

        input[type="number"] {
            width: calc(100% - 18px); /* Adjust width for number input */
        }

        button {
            background-color: #4caf50;
            color: #fff;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }

        a {
            display: block;
            margin-top: 10px;
            text-align: center;
            color: #333;
        }
    </style>
</head>
<body>
    <h1>Agregar Nuevo Producto</h1>

    <!-- Formulario para agregar un nuevo producto -->
    <form method="post" action="nuevo_producto.php">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required>

        <label for="codigo">Código:</label>
        <input type="text" id="codigo" name="codigo" required>

        <label for="marca">Marca:</label>
        <input type="text" id="marca" name="marca" required>

        <label for="precio">Precio:</label>
        <input type="number" id="precio" name="precio" step="0.01" required>

        <button type="submit">Agregar Producto</button>
    </form>

    <br>

    <a href="index.php">Volver al Listado de Productos</a>
</body>
</html>

<?php
// Configuración de la base de datos
define('DB_HOST', 'localhost');
define('DB_PORT', '5432');
define('DB_NAME', 'final');
define('DB_USER', 'postgres');
define('DB_PASSWORD', 'postgres');

session_start();

if (!isset($_SESSION['user_id'])) {
    header('Location: login.php');
    exit();
}

try {
    // Conexión a la base de datos
    $pdo = new PDO("pgsql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Error de conexión a la base de datos: " . $e->getMessage();
    die();
}

// Función para obtener y mostrar la lista de productos
function listarProductos($search = '') {
    global $pdo;

    try {
        // Consulta SQL para obtener productos con filtro de búsqueda
        $query = "SELECT * FROM productos";
        if (!empty($search)) {
            $query .= " WHERE nombre LIKE :search OR codigo LIKE :search OR marca LIKE :search";
        }
        $stmt = $pdo->prepare($query);
        if (!empty($search)) {
            $stmt->bindValue(':search', "%$search%");
        }
        $stmt->execute();
        $productos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Mostrar la grilla de productos
        echo "<table border='1'>";
        echo "<tr><th>Nombre</th><th>Código</th><th>Marca</th><th>Precio</th></tr>";
        foreach ($productos as $producto) {
            echo "<tr>";
            echo "<td>{$producto['nombre']}</td>";
            echo "<td>{$producto['codigo']}</td>";
            echo "<td>{$producto['marca']}</td>";
            echo "<td>{$producto['precio']}</td>";
            echo "</tr>";
        }
        echo "</table>";
    } catch (PDOException $e) {
        echo "Error al obtener la lista de productos: " . $e->getMessage();
    }
}

// Verificar si se envió un formulario de búsqueda
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $searchTerm = $_POST['search'];
    // Llamar a la función para listar productos con el término de búsqueda
    listarProductos($searchTerm);
}

// Verificar si se hizo clic en el botón de logout
if (isset($_POST['logout'])) {
    session_destroy(); // Cerrar la sesión
    header('Location: login.php'); // Redirigir al usuario a la página de login
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Listado de Productos</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
        }

        header {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        section {
            margin-top: 20px;
        }

        form {
            margin-bottom: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            padding: 10px;
            text-align: left;
            border: 1px solid #ddd;
        }

        button {
            padding: 8px;
            cursor: pointer;
            background-color: #4caf50;
            color: #fff;
            border: none;
        }

        button:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <header>
        <h1>Listado de Productos</h1>
    </header>

    <section id="search-form">
        <form method="post" action="index.php">
            <label for="search">Buscar:</label>
            <input type="text" id="search" name="search">
            <button type="submit">Buscar</button>
        </form>
    </section>

    <section id="logout-form">
        <form method="post" action="login.php">
            <button type="submit" name="logout">Cerrar Sesión</button><br>
        </form>
    </section>

    <section id="action-buttons">
        <a href="nuevo_producto.php"><button>Agregar Nuevo Producto</button></a><br>
        <a href="borrar_producto.php"><button>Borrar Producto</button></a>
        <a href="cambiar_contrasenha.php"><button>Cambiar Contraseña</button></a>
        <a href="modificar.php"><button>Modificar producto</button></a>
        <a href="producto_particular.php"><button>Visualizar producto</button></a>
    </section>

    <section id="product-list">
        <?php
        // Llamar a la función para listar productos sin filtro de búsqueda al cargar la página
        listarProductos();
        ?>
    </section>
</body>
</html>

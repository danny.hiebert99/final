<?php
// Configuración de la base de datos
$host = "localhost";
$port = "5432";
$dbname = "final";
$user = "postgres";
$password = "postgres";

// Conexión a la base de datos
$db = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

// Obtener la lista de productos
$stmtProductos = $db->query("SELECT id, nombre FROM productos");
$productos = $stmtProductos->fetchAll(PDO::FETCH_ASSOC);

// Inicializar variables para la información del producto seleccionado
$productoSeleccionado = null;
$mensaje = '';

// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener el ID del producto seleccionado
    $idProducto = $_POST["producto"];

    // Obtener los detalles del producto seleccionado
    $stmt = $db->prepare("SELECT * FROM productos WHERE id = :id");
    $stmt->bindParam(":id", $idProducto);
    $stmt->execute();

    $productoSeleccionado = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$productoSeleccionado) {
        $mensaje = "Producto no encontrado.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Visualizar Producto</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
            text-align: center;
        }

        h2, h3 {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        form {
            display: inline-block;
            text-align: left;
            max-width: 300px;
            margin: auto;
        }

        label {
            display: block;
            margin-top: 10px;
        }

        select, input[type="submit"] {
            padding: 8px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
        }

        h3 {
            margin-top: 20px;
        }

        p {
            margin-bottom: 10px;
        }

        a {
            display: block;
            margin-top: 10px;
            text-align: center;
            color: #333;
        }
    </style>
</head>
<body>

<h2>Visualizar Producto</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="producto">Selecciona un producto:</label>
    <select id="producto" name="producto">
        <?php foreach ($productos as $producto) : ?>
            <option value="<?php echo $producto["id"]; ?>"><?php echo $producto["nombre"]; ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <input type="submit" value="Visualizar Producto">
</form>

<?php if ($productoSeleccionado) : ?>
    <h3>Detalles del Producto</h3>
    <p>ID: <?php echo $productoSeleccionado["id"]; ?></p>
    <p>Nombre: <?php echo $productoSeleccionado["nombre"]; ?></p>
    <p>Código: <?php echo $productoSeleccionado["codigo"]; ?></p>
    <p>Marca: <?php echo $productoSeleccionado["marca"]; ?></p>
    <p>Precio: <?php echo $productoSeleccionado["precio"]; ?></p>
<?php elseif ($mensaje) : ?>
    <p><?php echo $mensaje; ?></p>
<?php endif; ?>

<br>
<br>

<a href="index.php">Volver al Listado de Productos</a>

</body>
</html>

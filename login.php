<?php
// Configuración de la base de datos
define('DB_HOST', 'localhost');
define('DB_PORT', '5432');
define('DB_NAME', 'final');
define('DB_USER', 'postgres');
define('DB_PASSWORD', 'postgres');

try {
    // Conexión a la base de datos
    $pdo = new PDO("pgsql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
    die();
}

// Función para verificar el inicio de sesión
function login($username, $password) {
    global $pdo;

    try {
        // Consulta SQL para verificar las credenciales del usuario
        $stmt = $pdo->prepare("SELECT id, username, password FROM usuarios WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        // Verificar la contraseña (en un entorno de producción, usar hash)
        if ($user && $user['password'] === $password) {
            // Iniciar sesión (aquí puedes establecer variables de sesión)
            session_start();
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['username'] = $user['username'];
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        echo "Error al iniciar sesión: " . $e->getMessage();
        return false;
    }
}

// Verificar si el formulario de inicio de sesión fue enviado
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Intentar iniciar sesión
    if (login($username, $password)) {
        echo "Inicio de sesión exitoso. Redirigiendo al dashboard...";
        header("Location: index.php");
        exit();
    } else {
        echo "Error: Credenciales inválidas.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
            text-align: center;
        }

        form {
            display: inline-block;
            text-align: left;
            max-width: 300px;
            margin: auto;
        }

        label {
            display: block;
            margin-top: 10px;
        }

        input {
            padding: 8px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
        }

        button {
            padding: 8px;
            cursor: pointer;
            background-color: #4caf50;
            color: #fff;
            border: none;
            width: 100%;
            box-sizing: border-box;
        }

        button:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <!-- Formulario de inicio de sesión -->
    <form method="post" action="login.php">
        <label for="username">Usuario:</label>
        <input type="text" id="username" name="username" required>

        <label for="password">Contraseña:</label>
        <input type="password" id="password" name="password" required>

        <button type="submit">Iniciar Sesión</button>
    </form>
</body>
</html>

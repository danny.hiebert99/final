<?php
// Configuración de la base de datos
$host = "localhost";
$port = "5432";
$dbname = "final";
$user = "postgres";
$password = "postgres";

// Conexión a la base de datos
$db = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener datos del formulario
    $id = $_POST["id"];
    $nombre = $_POST["nombre"];
    $codigo = $_POST["codigo"];
    $marca = $_POST["marca"];
    $precio = $_POST["precio"];

    // Actualizar el registro en la base de datos
    $stmt = $db->prepare("UPDATE productos SET nombre = :nombre, codigo = :codigo, marca = :marca, precio = :precio WHERE id = :id");
    $stmt->bindParam(":nombre", $nombre);
    $stmt->bindParam(":codigo", $codigo);
    $stmt->bindParam(":marca", $marca);
    $stmt->bindParam(":precio", $precio);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

    echo "Registro actualizado con éxito.";
}

// Obtener la lista de productos
$stmtProductos = $db->query("SELECT id, nombre FROM productos");
$productos = $stmtProductos->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modificar Producto</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
            text-align: center;
        }

        h2 {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        form {
            display: inline-block;
            text-align: left;
            max-width: 300px;
            margin: auto;
        }

        label {
            display: block;
            margin-top: 10px;
        }

        input, select {
            padding: 8px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #4caf50;
            color: #fff;
            border: none;
            cursor: pointer;
            padding: 8px;
        }

        input[type="submit"]:hover {
            background-color: #45a049;
        }

        a {
            display: block;
            margin-top: 10px;
            text-align: center;
            color: #333;
        }
    </style>
</head>
<body>

<h2>Modificar Producto</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="producto">Selecciona un producto:</label>
    <select id="producto" name="id">
        <?php foreach ($productos as $producto) : ?>
            <option value="<?php echo $producto["id"]; ?>"><?php echo $producto["nombre"]; ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" required>
    <br>
    <label for="codigo">Código:</label>
    <input type="text" id="codigo" name="codigo" required>
    <br>
    <label for="marca">Marca:</label>
    <input type="text" id="marca" name="marca" required>
    <br>
    <label for="precio">Precio:</label>
    <input type="text" id="precio" name="precio" required>
    <br>
    <input type="submit" value="Modificar Producto">
</form>
<br>
<br>
<a href="index.php">Volver al Listado de Productos</a>
</body>
</html>

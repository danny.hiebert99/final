<?php
// Configuración de la base de datos
$host = "localhost";
$port = "5432";
$dbname = "final";
$user = "postgres";
$password = "postgres";

// Conexión a la base de datos
$db = new PDO("pgsql:host=$host;port=$port;dbname=$dbname;user=$user;password=$password");

// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener datos del formulario
    $username = $_POST["username"];
    $newPassword = $_POST["newPassword"];

    // Verificar si el usuario existe
    $stmt = $db->prepare("SELECT id FROM usuarios WHERE username = :username");
    $stmt->bindParam(":username", $username);
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($row) {
        // Actualizar la contraseña del usuario
        $stmt = $db->prepare("UPDATE usuarios SET password = :password WHERE id = :id");
        $stmt->bindParam(":password", $newPassword);
        $stmt->bindParam(":id", $row["id"]);
        $stmt->execute();

        echo "Contraseña actualizada con éxito.";
    } else {
        echo "Usuario no encontrado.";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resetear Contraseña</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
        }

        h2 {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        form {
            margin-top: 20px;
            max-width: 300px;
            margin-left: auto;
            margin-right: auto;
        }

        label {
            margin-bottom: 10px;
            display: block;
        }

        input {
            padding: 8px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #4caf50;
            color: #fff;
            border: none;
            cursor: pointer;
            padding: 8px;
        }

        input[type="submit"]:hover {
            background-color: #45a049;
        }

        a {
            display: block;
            margin-top: 10px;
            text-align: center;
            color: #333;
        }
    </style>
</head>
<body>

<h2>Resetear Contraseña</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="username">Usuario:</label>
    <input type="text" id="username" name="username" required>
    <br>
    <label for="newPassword">Nueva Contraseña:</label>
    <input type="password" id="newPassword" name="newPassword" required>
    <br>
    <input type="submit" value="Resetear Contraseña">
    <br>
    <a href="index.php">Volver al Listado de Productos</a>
</form>

</body>
</html>

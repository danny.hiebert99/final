<?php
// Configuración de la base de datos
define('DB_HOST', 'localhost');
define('DB_PORT', '5432');
define('DB_NAME', 'final');
define('DB_USER', 'postgres');
define('DB_PASSWORD', 'postgres');

// Realizar la conexión a la base de datos
try {
    $pdo = new PDO("pgsql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Error de conexión a la base de datos: " . $e->getMessage();
    die();
}

// Verificar si se envió el formulario para borrar un producto
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['delete_product'])) {
    $product_id = $_POST['product_id'];

    // Validar y procesar el ID del producto antes de realizar la eliminación
    if (is_numeric($product_id)) {
        try {
            // Realizar la eliminación del producto
            $stmt = $pdo->prepare("DELETE FROM productos WHERE id = :id");
            $stmt->bindParam(':id', $product_id);
            $stmt->execute();

            echo "Producto eliminado correctamente.";
        } catch (PDOException $e) {
            echo "Error al eliminar el producto: " . $e->getMessage();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Borrar Producto</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
        }

        h1 {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        form {
            margin-top: 20px;
            display: flex;
            flex-direction: column;
            max-width: 300px;
        }

        label {
            margin-bottom: 10px;
        }

        input {
            padding: 8px;
            margin-bottom: 10px;
        }

        button {
            padding: 8px;
            cursor: pointer;
            background-color: #ff3333;
            color: #fff;
            border: none;
        }

        button:hover {
            background-color: #cc0000;
        }

        a {
            display: block;
            margin-top: 10px;
            text-align: center;
            color: #333;
        }
    </style>
</head>
<body>
    <h1>Borrar Producto</h1>

    <!-- Formulario para borrar un producto -->
    <form method="post" action="borrar_producto.php">
        <label for="product_id">ID del Producto a Borrar:</label>
        <input type="number" id="product_id" name="product_id" required>
        <button type="submit" name="delete_product">Borrar Producto</button>
    </form>

    <a href="index.php">Volver al Listado de Productos</a>
</body>
</html>
